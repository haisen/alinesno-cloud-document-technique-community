## 审计日志组件

<p class="show-images"><img src="/images/undraw_mobile_testing_reah.svg" width="40%" /></p>

### 场景

> 此为业务侵入性操作，使用场景有限制，不建议所有的日志都采集，建立针对性采集

针对的场景主要是审计日志，记录用户的操作行为、查询行为、点击行为、业务流水、
交易、数据库操作、系统的异常待进行记录，
配合业务系统审计处理。
考虑自建的目的是为了便于根据业务修改和后期的集成，同时为AIOPS提供一定的想像空间和团队经验积累。

### 原理
通过aop对应用进行切面，然后发送到kafka，日志组件接收kafka日志，并进行结构化存储，定期移到es中，同时保存时间建议是
3个月或者半年

### 功能列表
功能列表主要是对组件的管理功能，由管理员统一配置和监控，用户处理服务应用的异常记录，对开发人员透明

<p style="text-align:center"><img src="/images/14_watcher.png" width="100%" /></p>

##### 功能列表

| 功能名称 | 描述 | 进度 | 备注 |
|----------|------|------|------|
| 监控管理 |      |      |      |
| 服务健康 |      |      |      |
| 访问请求 |      |      |      |
| SQL操作  |      |      |      |
| 异常记录 |      |      |      |
| 业务日志 |      |      |      |
| 预警管理 |      |      |      |
| 参数设置 |      |      | .    |

### 埋点处理

添加依赖
```xml
<!-- agent_start -->
<dependency>
  <groupId>com.alinesno.cloud.monitor.agent</groupId>
  <artifactId>alinesno-cloud-watcher-agent</artifactId>
  <version>${最新版本}</version>
</dependency>
<!-- agent_end -->
```

添加注解
```java
import com.alinesno.cloud.monitor.agent.enable.EnableWatcher;

@EnableWatcher
```

### 其它
- 略
