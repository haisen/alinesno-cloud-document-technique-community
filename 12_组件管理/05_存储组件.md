## 存储组件

<p class="show-images"><img src="/images/undraw_co-working_825n.svg" width="40%" /></p>

### 概要
存储组件为平台提供存储服务，主要集成第三方存储，云存储（阿里云），还有本地存储服务，
并提供存储文件查询或可视化服务。服务提供Dubbo文件传输，Http文件传输两种方式，
便于业务组集成和研发。

### 功能列表

##### 云存储界面

<p style="text-align:center"><img src="/images/07_storage.png" width="100%" /></p>

##### 功能列表

| 功能              | 描述 | 状态   | 备注 |
|-------------------|------|--------|------|
| 存储统计          |      | 集成中 |      |
| 存储管理          |      | 集成中 |      |
| 存储历史          |      | 集成中 |      |
| 存储配置          |      | 集成中 |      |
| 七牛云存储集成    |      | 完成   |      |
| MinIO分布存储集成 |      | 完成   |      |
| 本地存储集成      |      | 完成   | .    |

### 使用说明

配置云存储路径
```yaml
# 存储路径按需调整
alinesno:
  storage:
    cloud.path: http://localhost:25003/storageFile/upload
```

接口调用
```java
String filepath = WebUploadUtils.transferFile(uploadCloudPath, dest , fileType , applicationName) ;
log.debug("filepath: {}" , filepath) ;
```

方法参数,适用于自定义封装的场景
```java
/**
 * 文件传输
 * @param cloudUrl
 * @param file
 * @return
 */
public static String transferFile(String cloudUrl , File file , String suffix , String applicationName){
      RestTemplate restTemplate = new RestTemplate();

      //设置请求头
      HttpHeaders headers = new HttpHeaders();
      MediaType type = MediaType.parseMediaType("multipart/form-data");
      headers.setContentType(type);

      //设置请求体，注意是LinkedMultiValueMap
      FileSystemResource fileSystemResource = new FileSystemResource(file);
      MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
      form.add("file", fileSystemResource);
      form.add("filename",file.getName());
      form.add("suffix", suffix);
      form.add("applicationName", applicationName);

      //用HttpEntity封装整个请求报文
      HttpEntity<MultiValueMap<String, Object>> files = new HttpEntity<>(form, headers);
      String s = restTemplate.postForObject(cloudUrl, files, String.class);

      return s ;
}
```

### 其它
- 略
